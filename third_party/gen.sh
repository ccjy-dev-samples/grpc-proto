#!/bin/bash

ver=v1

for i in $(ls api/proto/v1); do
    protoc --proto_path=api/proto/$ver \
        --go_out=../grpc-go \
        --go-grpc_out=../grpc-go \
        $i
done
